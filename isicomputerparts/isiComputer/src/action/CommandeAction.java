package action;

import javax.servlet.http.HttpServletRequest;

import manager.CommandeManager;

public class CommandeAction {
	public static void commander(HttpServletRequest request)
	{
		if(request.getSession().getAttribute("compte") != null && request.getSession().getAttribute("panier") != null)
		{
			CommandeManager.addCommande(request);
		}
		else
		{
			request.setAttribute("msgErreurCompte", "Erreur! Svp veuillez vous connecter a un compte");
		}
	}
}
