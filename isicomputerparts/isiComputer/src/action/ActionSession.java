package action;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ActionSession {

	public static boolean verifierPresenceCLef(HttpServletRequest request, String cle) {
		HttpSession session = request.getSession(false);

		boolean cleEstPresent = false;

		if (session != null) {
			Enumeration<String> listeCle = session.getAttributeNames();

			while (listeCle.hasMoreElements() && !cleEstPresent) {
				if (listeCle.nextElement().equals(cle)) {
					cleEstPresent = true;
				}
			}
		}

		return cleEstPresent;
	}

	public static void ajouterSession(HttpServletRequest request, String cle, Object objectToAddToSession) {
		HttpSession session = request.getSession(true);

		if (!verifierPresenceCLef(request, cle)) {
			session.setAttribute(cle, objectToAddToSession);
		}
	}
}
