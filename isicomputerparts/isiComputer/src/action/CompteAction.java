package action;

import javax.servlet.http.HttpServletRequest;

import entitee.Compte;
import manager.CompteManager;

public class CompteAction {
	/*public static boolean login(HttpServletRequest request, String pseudo, String password)
	{
		boolean loginSuccessful = false;
		
		if(verifyCompte(pseudo, password))
		{
			loginSuccessful = true;
			Compte compte = getCompte(pseudo);
			
		}
		
		return loginSuccessful;
	}*/
	public static void inscription(HttpServletRequest request)
	{
		String msgErreur = "";
		if(request.getParameter("nom") != null && request.getParameter("prenom") != null && request.getParameter("pseudo") != null  && request.getParameter("mail") != null && request.getParameter("password") != null)
		{
			if(CompteManager.verifyPseudoIsUnique(request.getParameter("pseudo")))
			{
				CompteManager.AddCompte(request.getParameter("pseudo"), request.getParameter("password"), request.getParameter("mail"), request.getParameter("nom"), request.getParameter("prenom"));
				Compte compte = CompteManager.getCompte(request.getParameter("pseudo"));
				if(compte != null)
				{
					request.getSession().setAttribute("compte", compte);
				}
			}
			else
			{
				msgErreur = "Ce pseudo existe deja ! Svp en choisir un autre";
			}
		}
		else
		{
			msgErreur = "Tous les champs du formulaire inscription doit etre rempli";
		}
		if(msgErreur != "")
			request.setAttribute("msgErreur", msgErreur);
	}
	public static void connexion(HttpServletRequest request)
	{
		String msgErreurConnexion = "";
		if(request.getParameter("pseudo") != null && request.getParameter("password") != null )
		{
			if(CompteManager.verifyCompte(request.getParameter("pseudo"),request.getParameter("password")))
			{
				Compte compte = CompteManager.getCompte(request.getParameter("pseudo"));
				if(compte != null)
				{
					request.getSession().setAttribute("compte", compte);
				}
			}
			else
			{
				msgErreurConnexion = "Le pseudo ou le mot de passe est erronner! Svp recommencer";
			}
		}
		else
		{
			msgErreurConnexion = "Tous les champs du formulaire connexion doit etre rempli";
		}
		if(msgErreurConnexion != "")
			request.setAttribute("msgErreurConnexion", msgErreurConnexion);
	}
}
