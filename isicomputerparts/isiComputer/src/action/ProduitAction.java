package action;

import manager.CategorieManager;
import manager.ProduitManager;

import javax.servlet.http.HttpServletRequest;

public class ProduitAction {
	public static void afficherTousLesProduits(HttpServletRequest request) {
		request.setAttribute("produit", ProduitManager.getAll());
	}

	public static void afficherProduitsParId(HttpServletRequest request, int id) {
		request.setAttribute("idProduit", ProduitManager.getById(id));
	}
	public static void afficherProduitsParCategorie(HttpServletRequest request, int id) {
		request.setAttribute("produit", ProduitManager.getByIdCategorie(id));
	}
	public static void afficherProduitsParIdNomsCategories(HttpServletRequest request,int categories, String name) {
		request.setAttribute("produit", ProduitManager.getByNameAndIdCategorie(categories,name));
	}
	public static void afficherProduitsParNoms(HttpServletRequest request, String name) {
		request.setAttribute("produit", ProduitManager.getByName(name));
	}
}
