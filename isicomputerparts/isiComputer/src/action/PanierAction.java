package action;



import javax.servlet.http.HttpServletRequest;

import manager.PanierManager;

public class PanierAction {
	public static void AddPanier(HttpServletRequest request, int id) {
		PanierManager.add(id);
		request.getSession().setAttribute("panier", PanierManager.items);
	}
	
	
	public static void removePanier(HttpServletRequest request, int id) {

		PanierManager.remove(id);
		request.getSession().setAttribute("panier", PanierManager.items);
		if(PanierManager.items.size() == 0)
		{
			request.getSession().removeAttribute("panier");
		}
	}
}
