package entitee;

public class Compte {

	private int idCompte;
	private String pseudo;
	private String mdp;
	private String email;
	private String nom;
	private String prenom;
	public Compte() {

	}
	public Compte(int idCompte, String pseudo, String mdp, String email, String nom, String prenom) {
		super();
		this.idCompte = idCompte;
		this.pseudo = pseudo;
		this.mdp = mdp;
		this.email = email;
		this.nom = nom;
		this.prenom = prenom;
	}
	public int getIdCompte() {
		return idCompte;
	}
	public void setIdCompte(int idCompte) {
		this.idCompte = idCompte;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}
