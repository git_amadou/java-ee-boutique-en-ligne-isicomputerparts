package entitee;

public class ItemPanier {
	private Produit produit;
	private int qty;

	public ItemPanier() {
		super();
	}

	public ItemPanier(Produit produit) {
		super();
		this.produit = produit;
		this.qty = 1;
	}

	

    public void incrementQuantity() {
    	qty++;
    }

    public void decrementQuantity() 
    {
    	qty--;
    }
	
	public Produit getProduit() {
		return produit;
	}
/*
	public void setIdProduit(int idProduit) {
		this.idProduit = idProduit;
	}*/

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

}
