package entitee;

public class Produit {

	private int id;
	private String nom;
	private String titre;
	private String description;
	private double prix;
	private String categorie; 
	private int idCategorie; 
	
	public Produit() {
		
	}

	public int getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(int idcategorie) {
		this.idCategorie = idcategorie;
	}

	public Produit(int id, String nom, String description, double prix) {
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.prix = prix;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

}
