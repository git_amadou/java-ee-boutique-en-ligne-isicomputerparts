package controler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.CommandeAction;
import manager.emailManager;
/**
 * Servlet implementation class CommanderControler
 */
@WebServlet("/commanderControler")
public class CommanderControler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommanderControler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			CommandeAction.commander(request);
			if(request.getAttribute("msgErreurCompte") != null)
			{
				request.getRequestDispatcher("panierControler").forward(request, response);
			}
			else
			{
				emailManager.sendEmail(request);
				request.setAttribute("commandeReussi", "Votre commande a ete envoyer avec success");
				request.getSession().removeAttribute("panier");
				request.getRequestDispatcher("produitControler").forward(request, response);
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
