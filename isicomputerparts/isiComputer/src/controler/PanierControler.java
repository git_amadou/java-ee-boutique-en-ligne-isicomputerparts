package controler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.CategorieAction;

/**
 * Servlet implementation class PanierControler
 */
@WebServlet("/panierControler")
public class PanierControler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PanierControler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//String paramValue = request.getParameter("boutonPanier");
		CategorieAction.afficherTousLesCatgeories(request);
		if(request.getParameter("paramValue") != null)
		{
			int paramId = Integer.parseInt(request.getParameter("idProduit"));
			if(request.getParameter("paramValue") != null)
			{
				switch (request.getParameter("paramValue")) {
				case "+":
					action.PanierAction.AddPanier(request, paramId);
					break;
				case "-":
					action.PanierAction.removePanier(request, paramId);
					break;
				default:
					break;
				}
			
			}
			
		}
		else if(request.getParameter("idProduit") != null)
		{
			int paramId = Integer.parseInt(request.getParameter("idProduit"));
			action.PanierAction.AddPanier(request, paramId);
			
		}
		else if(request.getParameter("commander") != null)
		{
			
		}
		request.getRequestDispatcher("panier.jsp").forward(request, response);
			
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}
	

}
