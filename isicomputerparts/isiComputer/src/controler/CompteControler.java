package controler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.CompteAction;

/**
 * Servlet implementation class CompteControler
 */
@WebServlet("/compteControler")
public class CompteControler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompteControler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("inscription") != null)
		{
			CompteAction.inscription(request);
			if(request.getSession().getAttribute("compte") != null)
			{
				request.getRequestDispatcher("produitControler").forward(request, response);
			}
			else
			{
				request.getRequestDispatcher("inscription.jsp").forward(request, response);
			}
		}
		else if(request.getParameter("connexion") != null)
		{
			CompteAction.connexion(request);
			if(request.getSession().getAttribute("compte") != null)
			{
				request.getRequestDispatcher("produitControler").forward(request, response);
			}
			else
			{
				request.getRequestDispatcher("inscription.jsp").forward(request, response);
			}
		}
		else if(request.getParameter("deco") != null)
		{
			request.getSession().removeAttribute("compte");
			request.getRequestDispatcher("produitControler").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
