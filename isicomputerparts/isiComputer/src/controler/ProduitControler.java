package controler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.CategorieAction;
import action.ProduitAction;
import entitee.Produit;

/**
 * Servlet implementation class ProduitControler
 */
@WebServlet("/produitControler")
public class ProduitControler extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ProduitControler() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		CategorieAction.afficherTousLesCatgeories(request);
		if(request.getParameter("categorie") != null && request.getParameter("recherche").equals(""))
		{
			if(request.getParameter("categorie").equals("0"))
			{
				ProduitAction.afficherTousLesProduits(request);
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
			else
			{
				ProduitAction.afficherProduitsParCategorie(request, Integer.parseInt(request.getParameter("categorie")));
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		}
		else if(request.getParameter("categorie") != null && !request.getParameter("recherche").equals(""))
		{
			if(request.getParameter("categorie").equals("0"))
			{
				ProduitAction.afficherProduitsParNoms(request, request.getParameter("recherche"));
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
			else
			{
				ProduitAction.afficherProduitsParIdNomsCategories(request, Integer.parseInt(request.getParameter("categorie")), request.getParameter("recherche"));
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		}
		else if(request.getParameter("idProduit") == null)
		{
			ProduitAction.afficherTousLesProduits(request);
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		else
		{
			ProduitAction.afficherProduitsParId(request, Integer.parseInt(request.getParameter("idProduit")));
			request.getRequestDispatcher("detail.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
