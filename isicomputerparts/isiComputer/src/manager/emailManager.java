package manager;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
 
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import entitee.Compte;
import entitee.ItemPanier;

public class emailManager {
	 public static void sendEmail(HttpServletRequest request) {
		 HashMap<Integer, ItemPanier> panier = (HashMap)(request.getSession().getAttribute("panier"));
		 String mailContent = "";
		 double montantTotal = 0;
		 mailContent += "<h1>Liste de la commande effectuer</h1>";
		 mailContent += "<table >";
		 mailContent += "<tr>";
		 mailContent += "<th>Nom</th>";
		 mailContent += "<th>Quantiter</th>";
		 mailContent += "<th>Prix</th>";
		 mailContent += "</tr>";
		for(Map.Entry<Integer, ItemPanier> e : panier.entrySet())
		{
			mailContent += "<tr>";
			mailContent += "<td>" + e.getValue().getProduit().getTitre() + "</td>";
			mailContent += "<td>" + e.getValue().getQty() + "</td>";
			mailContent += "<td>" + e.getValue().getProduit().getPrix() * e.getValue().getQty() + "</td>";
			mailContent += "</tr>";
		}
			
		mailContent += "</table>";
		mailContent +="<p>Le montant total est de </p>" + PanierManager.getTotal() + "$";
		 final String username = "isicomputerparts@gmail.com";
		 final String password = "isiabc123...";

	        Properties prop = new Properties();
			prop.put("mail.smtp.host", "smtp.gmail.com");
	        prop.put("mail.smtp.port", "587");
	        prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.starttls.enable", "true"); //TLS
	        
	        Session session = Session.getInstance(prop,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(username, password);
	                    }
	                });

	        try {

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(username));
	            message.setRecipients(
	                    Message.RecipientType.TO,
	                    InternetAddress.parse(((Compte)request.getSession().getAttribute("compte")).getEmail())
	            );
	            message.setSubject("test mail");
	            message.setContent(mailContent, "text/html; charset=utf-8");

	            Transport.send(message);
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
	 
	    }
}
