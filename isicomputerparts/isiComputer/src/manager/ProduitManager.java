package manager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entitee.Produit;
import service.*;;

public class ProduitManager {
	
	private static String requeteGetAllProduit = "select idProduit, p.nom , prix, titre, c.Nom as nomCategorie, description, c.idCategorie from produit p join categorie c on p.idCategorie = c.idCategorie order by prix desc;";
	private static String requeteGetByIdCategorie = "select idProduit, p.nom , prix, titre, c.Nom as nomCategorie, description, c.idCategorie from produit p join categorie c on p.idCategorie = c.idCategorie Where p.idCategorie=?;";
	private static String requeteGetById = "Select * From produit Where idProduit=?;";
	private static String requeteGetByNameAndCategorie = "select idProduit, p.nom , prix, titre, c.Nom as nomCategorie, description, c.idCategorie from produit p join categorie c on p.idCategorie = c.idCategorie Where p.nom like ? AND p.idCategorie=?;";
	private static String requeteGetByName = "select idProduit, p.nom , prix, titre, c.Nom as nomCategorie, description, c.idCategorie from produit p join categorie c on p.idCategorie = c.idCategorie Where p.nom like ?;";

	public static ArrayList<Produit> getAll() {
		ArrayList<Produit> produit = new ArrayList<>();
		
		try {
			
			PreparedStatement ps = ConnexionBD.getPs(requeteGetAllProduit);
			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Produit prod = new Produit();
				prod.setId(result.getInt("idProduit"));
				prod.setPrix(result.getDouble("prix"));
				prod.setTitre(result.getString("titre"));
				prod.setCategorie(result.getString("nomCategorie"));
				prod.setIdCategorie(result.getInt("idCategorie"));

				produit.add(prod);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();	
		return produit;
	}
	
	public static ArrayList<Produit> getByIdCategorie(int id){
		
		ArrayList<Produit> produit = new ArrayList<>();
		
		try {
			PreparedStatement ps = ConnexionBD.getPs(requeteGetByIdCategorie);
			ps.setInt(1, id);
			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Produit prod = new Produit();
				prod.setId(result.getInt("idProduit"));
				prod.setPrix(result.getDouble("prix"));
				prod.setTitre(result.getString("titre"));
				prod.setCategorie(result.getString("nomCategorie"));
				prod.setIdCategorie(result.getInt("idCategorie"));

				produit.add(prod);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();	
		return produit;
	}
	
	public static Produit getById(int id){
		
		Produit produit = null;
		
		try {
			PreparedStatement ps = ConnexionBD.getPs(requeteGetById);
			ps.setInt(1, id);
			ResultSet result = ps.executeQuery();

			result.next();
			produit = new Produit();
			produit.setDescription(result.getString("description"));
			produit.setId(result.getInt("idProduit"));
			produit.setNom(result.getString("nom"));
			produit.setPrix(result.getDouble("prix"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();	
		return produit;
	}
public static ArrayList<Produit> getByName(String name){
		
		ArrayList<Produit> produit = new ArrayList<>();
		
		try {
			PreparedStatement ps = ConnexionBD.getPs(requeteGetByName);
			ps.setString(1, "%" + name + "%");
			

			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Produit prod = new Produit();
				prod.setId(result.getInt("idProduit"));
				prod.setPrix(result.getDouble("prix"));
				prod.setTitre(result.getString("titre"));
				prod.setCategorie(result.getString("nomCategorie"));
				prod.setIdCategorie(result.getInt("idCategorie"));
				produit.add(prod);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();	
		return produit;
	}
	public static ArrayList<Produit> getByNameAndIdCategorie(int categorie, String name){
		
		ArrayList<Produit> produit = new ArrayList<>();
		
		try {
			PreparedStatement ps = ConnexionBD.getPs(requeteGetByNameAndCategorie);
			ps.setString(1, "%" + name + "%");
			ps.setInt(2, categorie);
			

			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Produit prod = new Produit();
				prod.setId(result.getInt("idProduit"));
				prod.setPrix(result.getDouble("prix"));
				prod.setTitre(result.getString("titre"));
				prod.setCategorie(result.getString("nomCategorie"));
				prod.setIdCategorie(result.getInt("idCategorie"));
				produit.add(prod);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();	
		return produit;
	}

}
