package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import entitee.ItemPanier;
import entitee.Produit;
import service.ConnexionBD;

public class PanierManager { 
	public static HashMap<Integer, ItemPanier> items = new HashMap<Integer, ItemPanier>();
	static int numberOfItems = 0;


	public static void add(int idProduit) 
	{
		Produit prod = new Produit();
		try {
			PreparedStatement ps = ConnexionBD.getPs("select * from produit where idProduit=?");
			ps.setInt(1, idProduit);
			ResultSet result = ps.executeQuery();

			while (result.next()) {
				prod.setId(result.getInt("idProduit"));
				prod.setPrix(result.getDouble("prix"));
				prod.setTitre(result.getString("titre"));
				prod.setIdCategorie(result.getInt("idCategorie"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();
		try
		{
			items.get(idProduit).incrementQuantity();
		} catch (Exception e) {
			ItemPanier newItem = new ItemPanier(prod);
			items.put(idProduit, newItem);
		}
	}

	public static void remove(int idProduit) {
		if (items.containsKey(idProduit)) {
			items.get(idProduit).decrementQuantity();

			if (items.get(idProduit).getQty() <= 0) {
				items.remove(idProduit);
			}

		}
	}

	public static List<ItemPanier> getItems() {
		List<ItemPanier> results = new ArrayList<>();
		results.addAll(PanierManager.items.values());

		return results;
	}

	public static double getTotal() {
		double amount = 0.0;
		for(Map.Entry<Integer, ItemPanier> e : items.entrySet())
		{
			amount += e.getValue().getQty() * e.getValue().getProduit().getPrix();
		}

		return roundOff(amount);
	}

	public static double roundOff(double x) {
		long val = Math.round(x * 100); // cents

		return val / 100.0;
	}
}
