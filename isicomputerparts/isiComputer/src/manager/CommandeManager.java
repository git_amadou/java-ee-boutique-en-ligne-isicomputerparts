package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import entitee.Compte;
import entitee.ItemPanier;
import service.ConnexionBD;

public class CommandeManager {
	private static String requeteInsertVente = "INSERT INTO vente (idVente, date, idCompte) value (?, now(), ?)";
	private static String requeteGetMaxId = "select max(idVente) as max from vente";
	private static String requeteInsertCommande = "INSERT INTO commande (idVente, idProduit, qty) value (?, ?, ?)";
	public static void addCommande(HttpServletRequest request)
	{
		int vente = 0;
		try {
			HashMap<Integer, ItemPanier> panier = (HashMap)request.getSession().getAttribute("panier");
					
			PreparedStatement ps = ConnexionBD.getPs(requeteGetMaxId);
			ResultSet result = ps.executeQuery();
			while (result.next()) {
				vente = result.getInt("max") + 1;
			}
			ConnexionBD.close();
			ps = ConnexionBD.getPs(requeteInsertVente);
			ps.setInt(1, vente);
			ps.setInt(2, ((Compte)request.getSession().getAttribute("compte")).getIdCompte());
			ps.executeUpdate();
			ConnexionBD.close();
			for(Map.Entry<Integer, ItemPanier> e : panier.entrySet())
			{
				ps = ConnexionBD.getPs(requeteInsertCommande);
				ps.setInt(1, vente);
				ps.setInt(2, e.getValue().getProduit().getId());
				ps.setInt(3, e.getValue().getQty());
				ps.executeUpdate();
				ConnexionBD.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();
	}
}
