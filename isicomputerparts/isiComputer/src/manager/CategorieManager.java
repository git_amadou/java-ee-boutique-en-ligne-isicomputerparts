package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entitee.Categorie;
import service.ConnexionBD;

public class CategorieManager {

	private static String requeteGetAllCategorie = "Select * From categorie;";

	public static ArrayList<Categorie> getAll() {
		ArrayList<Categorie> categorie = new ArrayList<>();

		try {
			PreparedStatement ps = ConnexionBD.getPs(requeteGetAllCategorie);
			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Categorie cat = new Categorie();
				cat.setId(result.getInt("idCategorie"));
				cat.setNom(result.getString("Nom"));
				categorie.add(cat);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();

		return categorie;
	}
}
