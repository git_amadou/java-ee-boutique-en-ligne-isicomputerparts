package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entitee.Compte;
import service.ConnexionBD;

public class CompteManager {

	private static String queryAddCompte = "insert into compte (pseudo, mdp, email, nom, prenom) value (?, ?, ?, ?, ?);";
	private static String querySelectAllCompte = "Select * From compte";
	private static String querySelectionnerUnCompte = "Select * From compte Where pseudo=?";

	public static ArrayList<Compte> getAll() {
		ArrayList<Compte> listeUsers = new ArrayList<Compte>();
		try {
			
			PreparedStatement ps = ConnexionBD.getPs(querySelectAllCompte);
			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Compte comp = new Compte();
				comp.setEmail(result.getString("prenom"));
				comp.setIdCompte(result.getInt("idCompte"));
				comp.setMdp(result.getString("mdp"));
				comp.setNom(result.getString("nom"));
				comp.setPrenom(result.getString("prenom"));
				comp.setPseudo(result.getString("pseudo"));

				listeUsers.add(comp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConnexionBD.close();	
		return listeUsers;
	}
	
	public static void AddCompte(String pseudo, String mdp, String email, String nom, String prenom)
	{
		try {
			PreparedStatement ps = ConnexionBD.getPs(queryAddCompte);
			
			ps.setString(1, pseudo);
			ps.setString(2, mdp);
			ps.setString(3, email);
			ps.setString(4, nom);
			ps.setString(5, prenom);
			ps.executeUpdate();
			

					} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		ConnexionBD.close();
		
	}
	
	
	public static boolean verifyCompte(String pseudo, String mdp)
	{
		ArrayList<Compte> listeUser = getAll();
		
		for(Compte compte : listeUser)
		{
			if(compte.getPseudo().equals(pseudo) && compte.getMdp().equals(mdp))
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	public static boolean verifyPseudoIsUnique(String pseudo)
	{
		ArrayList<Compte> listeUser = getAll();
		
		for(int i = 0; i < listeUser.size(); i++)
		{
			if(listeUser.get(i).getPseudo().equals(pseudo))
			{
				return false;
			}
		}
		
		return true;
	}
	
	public static Compte getCompte(String pseudo)
	{
		Compte compte = null;
		
		PreparedStatement ps = ConnexionBD.getPs(querySelectionnerUnCompte);
		ResultSet resultSet = null;
		
		try {
			ps.setString(1, pseudo);
			resultSet = ps.executeQuery();
			
			if(resultSet.isBeforeFirst()) {
				while(resultSet.next()) {
					int idCompte = resultSet.getInt("idCompte");
					String pseudo1 =resultSet.getString("pseudo");
					String mdp =resultSet.getString("mdp");
					String email =resultSet.getString("email");
					String nom =resultSet.getString("nom");
					String prenom =resultSet.getString("prenom");					
								
					compte = new Compte(idCompte, pseudo1, mdp, email, nom, prenom);
				}
			}
		}
		catch (SQLException e) {
			
		}
		ConnexionBD.close();
	
 		return compte;
	}
	
}